
# coding: utf-8

# In[20]:


import pandas as pd
import math
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_score
from sklearn import tree
import numpy as np
import xlsxwriter
import matplotlib.pyplot as plt


# In[2]:


cd desktop


# In[3]:


train = pd.read_excel('task1.xlsx', sheet_name = 'TRAIN')
test = pd.read_excel('task1.xlsx', sheet_name = 'TEST')


# In[5]:


from sklearn import linear_model


# In[6]:


def handle_non_numerical_data (train):
    columns = train.columns.values
    
    for column in columns:
        text_digit_vals = {}
        def convert_to_int(val):
            return text_digit_vals[val]
        
        if train[column].dtype != np.int64 and train[column].dtype !=np.float64:
            column_contents = train[column].values.tolist()
            unique_elements = set(column_contents)
            x = 0
            for unique in unique_elements:
                if unique not in text_digit_vals:
                    text_digit_vals[unique] = x
                    x+=1
                    
            train[column] = list(map(convert_to_int, train[column]))
            
    return train

train_clean = handle_non_numerical_data(train)
train_clean.head()


# In[7]:


def handle_non_numerical_data_test (test):
    columns = test.columns.values
    
    for column in columns:
        text_digit_vals = {}
        def convert_to_int(val):
            return text_digit_vals[val]
        
        if test[column].dtype != np.int64 and test[column].dtype !=np.float64:
            column_contents = test[column].values.tolist()
            unique_elements = set(column_contents)
            x = 0
            for unique in unique_elements:
                if unique not in text_digit_vals:
                    text_digit_vals[unique] = x
                    x+=1
                    
            test[column] = list(map(convert_to_int, test[column]))
            
    return test

test_clean = handle_non_numerical_data_test(test)
test_clean.head()


# In[15]:


#factors that will predict the price
desired_factors = ['YEAR','ENGINE_VOLUME','FUEL_TYPE','BODY_TYPE','TYPE_OF_DRIVE','INTERIOR_TYPE','TRANSM_TYPE','AUTO_CONDITION','AVG_COST']


#set prediction data to factors that will predict, and set target to SalePrice
train_data = train_clean[desired_factors]
test_data = test_clean[desired_factors]
target = train_clean.ESTIM_COST


# In[16]:


regr = linear_model.LinearRegression()
regr.fit(train_data, target)
LinearRegression(copy_X=True, fit_intercept=True, n_jobs=1, normalize=False)
print(regr.coef_)


 # The mean square error


# Explained variance score: 1 is perfect prediction
 # and 0 means that there is no linear relationship
# between X and y.
#regr.score(train_data, test_data) 


# In[23]:


target.head(50)


# In[25]:


len(test_data.YEAR)


# In[26]:


len(target)


# In[32]:



l = regr.predict(test_data)
print (l)


# In[33]:


test_data['ESTIM_COST'] = l


# In[38]:


test_data.head(50)


# In[35]:


test_data.ESTIM_COST.value_counts()


# In[36]:


test_data['differ'] = (1-test_data['ESTIM_COST']/test_data['AVG_COST'])*100

test_data['differ'].mean()


# In[37]:


len(test_data[test_data['differ']>10])


# In[39]:


import statsmodels.api as sm

# Note the difference in argument order
model = sm.OLS(target, train_data).fit()
predictions = model.predict(train_data) # make the predictions by the model

# Print out the statistics
model.summary()


# In[41]:


train_clean['differ'] = (1-train_clean['ESTIM_COST']/train_clean['AVG_COST'])*100

train_clean['differ'].mean()


# In[42]:


len(train_clean[train_clean['differ']>10])


# In[43]:


len(train_clean[train_clean['differ']<0])


# In[49]:


train_required = train_clean.ix[(train_clean['differ']<10), ['YEAR','ENGINE_VOLUME','FUEL_TYPE','BODY_TYPE','TYPE_OF_DRIVE','INTERIOR_TYPE','TRANSM_TYPE','AUTO_CONDITION','AVG_COST', 'differ']]


# In[50]:


train_required.head(50)


# In[51]:


train_required = train_clean.ix[(train_clean['differ']>0), ['YEAR','ENGINE_VOLUME','FUEL_TYPE','BODY_TYPE','TYPE_OF_DRIVE','INTERIOR_TYPE','TRANSM_TYPE','AUTO_CONDITION','AVG_COST', 'differ']]


# In[52]:


train_required.head(50)


# In[61]:


train_required = train_clean.ix[(train_clean['differ']<10)&(train_clean['differ']>-10), ['YEAR','ENGINE_VOLUME','FUEL_TYPE','BODY_TYPE','TYPE_OF_DRIVE','INTERIOR_TYPE','TRANSM_TYPE','AUTO_CONDITION','AVG_COST','ESTIM_COST', 'differ']]


# In[62]:


train_required.head()


# In[79]:


#set prediction data to factors that will predict, and set target to SalePrice
train_data_req = train_required[desired_factors]
#test_data = test_clean[desired_factors]
target_req = train_required.ESTIM_COST

regr = linear_model.LinearRegression()
regr.fit(train_data_req, target_req)
LinearRegression(copy_X=True, fit_intercept=True, n_jobs=1, normalize=False)
print(regr.coef_)


# In[67]:




# Note the difference in argument order
model = sm.OLS(target_req, train_data_req).fit()
predictions = model.predict(train_data_req) # make the predictions by the model

# Print out the statistics
model.summary()


# In[70]:


train_data_req.head()


# In[72]:


test_clean.head()


# In[75]:


target_req.head()


# In[77]:


desired_factors = ['YEAR','ENGINE_VOLUME','FUEL_TYPE','BODY_TYPE','TYPE_OF_DRIVE','INTERIOR_TYPE','TRANSM_TYPE','AUTO_CONDITION','AVG_COST']


#set prediction data to factors that will predict, and set target to SalePrice

test_data = test_clean[desired_factors]


# In[78]:


test_data.head()


# In[85]:


m = regr.predict(test_data)
print (m)


# In[87]:


test_data.head(100)


# In[89]:


test_data['differ'] = (1-test_data['ESTIM_COST']/test_data['AVG_COST'])*100

test_data['differ'].mean()


# In[90]:


writer = pd.ExcelWriter('task1.xlsx', engine='xlsxwriter')


# In[91]:


test_data.to_excel(writer, sheet_name='TEST')


# In[92]:


writer.save()

