# Linear_Regression_CAR_COST_ESTIMATION

This project is a competition for job interview of one of the Kazakhstan Bank.

First stage was a test about excel, SQL and basic data science models.

Second stage was a task: to estimate Cost of the cars from task1.xslx file with TRAIN sheet and TEST sheet. 

I used Linear Regression of sklearn in order to predict ESTIM_COST